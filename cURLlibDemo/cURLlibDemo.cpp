// cURLlibDemo.cpp : 定义控制台应用程序的入口点。
//

#include "stdafx.h"

#include <string>

#include "../libcURL/include/curl.h"
#ifdef _DEBUG
	#pragma comment(lib, "../libcURL/lib/libcurld.lib")
#else
	#pragma comment(lib, "../libcURL/lib/libcurl.lib")
#endif

#include "kutfconv.h"

// ftp上传进度
long g_lFileSize = 0;
long g_lUploadSize = 0;

// ftp文件上传
bool UploadFileFTP(LPCTSTR szFile);

static size_t GetPageString(void *ptr, size_t size, size_t nmemb, void *userdata)
{
	//参数userdata是存放数据的指针  其他三个获取内容
	std::string *strHtml = (std::string *)userdata;
	strHtml->append((char*)ptr, size * nmemb);
	return (size * nmemb);
}

void UploadFile(std::string &strRet)
{
	CURL *curl;
    CURLcode res;
 
	struct curl_httppost *formpost=NULL;
	struct curl_httppost *lastptr=NULL;
	struct curl_slist *headerlist=NULL;
	//static const char buf[] = "Expect:";
	
	curl_formadd(&formpost, &lastptr, 
		CURLFORM_COPYNAME, "jkid", 
		CURLFORM_COPYCONTENTS, "Hzhbjkw014", 
		CURLFORM_CONTENTTYPE, "text/plain", 
		CURLFORM_END);
	curl_formadd(&formpost, &lastptr, 
		CURLFORM_COPYNAME, "jyjgbm", 
		CURLFORM_COPYCONTENTS, "33010502", 
		CURLFORM_CONTENTTYPE, "text/plain", 
		CURLFORM_END);
	curl_formadd(&formpost, &lastptr, 
		CURLFORM_COPYNAME, "sqm", 
		CURLFORM_COPYCONTENTS, "33010502", 
		CURLFORM_CONTENTTYPE, "text/plain", 
		CURLFORM_END);
	curl_formadd(&formpost, &lastptr, 
		CURLFORM_COPYNAME, "nodecode", 
		CURLFORM_COPYCONTENTS, "1001", 
		CURLFORM_CONTENTTYPE, "text/plain", 
		CURLFORM_END);
	curl_formadd(&formpost, &lastptr, 
		CURLFORM_COPYNAME, "jsonData", 
		CURLFORM_COPYCONTENTS, "{\
								   \"body\" : [\
									  {\
										 \"cursor\" : \"01\",\
										 \"filename\" : \"330105020120170606162732_01.jpg\",\
										 \"hphm\" : \"%E6%B5%99A34088\",\
										 \"hpys\" : \"2\",\
										 \"sqsj\" : \"\"\
									  }\
								   ],\
								   \"exchangeCode\" : \"33010502201706070825002017060733010502\",\
								   \"exchangeType\" : \"10\",\
								   \"jcjgbm\" : \"33010502\",\
								   \"nodecode\" : \"1001\",\
								   \"requestTime\" : \"20170607082500\",\
								   \"source\" : \"1\",\
								   \"sqm\" : \"33010502\"\
								}", 
		CURLFORM_CONTENTTYPE, "text/plain", 
		CURLFORM_END);

	/* Fill in the file upload field */ 
	curl_formadd(&formpost, &lastptr, 
		CURLFORM_COPYNAME, "sendfile", 
		CURLFORM_FILE, "pci.png", 
		CURLFORM_END);

	/* Fill in the filename field */ 
	curl_formadd(&formpost, &lastptr, 
		CURLFORM_COPYNAME, "filename", 
		CURLFORM_COPYCONTENTS, "pci.png", 
		CURLFORM_END);
	
	/* Fill in the submit field too, even if this is rarely needed */ 
	curl_formadd(&formpost, &lastptr, 
		CURLFORM_COPYNAME, "submit", 
		CURLFORM_COPYCONTENTS, "send", 
		CURLFORM_END);

	curl = curl_easy_init();    // 初始化
	/* initalize custom header list (stating that Expect: 100-continue is not wanted */ 
	headerlist = curl_slist_append(headerlist, "Expect:");
	
	if (curl)
	{
		curl_easy_setopt(curl, CURLOPT_URL, "http://169.254.41.46:8080");
		//curl_easy_setopt(curl, CURLOPT_URL, "http://218.108.60.66:8080/mds_exg/work/nozzle/fesave.do");
 
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
		curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);
				
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &strRet);	//将返回的html主体数据输出到fp指向的文件		
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, GetPageString);	//设置一个回掉函数来获取数据

		//这句话的意思是，设置超时时间，如果服务器在120秒不返回，Curl就会触发一个TimeOUT错误。
		//这里建议设置，防止你的代码在某些特殊时刻无限的等待服务器的返回。
		curl_easy_setopt(curl, CURLOPT_TIMEOUT, 10);

		res = curl_easy_perform(curl);   // 执行
		if (res == CURLE_OK) 
		{ 
			printf("上传完成\r\n");
		} 
		else
		{ 
			printf("上传失败\r\n");
		}  

		curl_easy_cleanup(curl);   
		/* then cleanup the formpost chain */ 
		curl_formfree(formpost);
		curl_slist_free_all(headerlist);  
	}

	// 已Post上传数据
	// http://218.108.60.66:85/group1/M00/00/01/CiEbR1k4IdeAMTxBAAKJso9XEXY002.png
}

void UploadFileXJ(std::string &strRet)
{
	CURL *curl;
	CURLcode res;

	struct curl_httppost *formpost = NULL;
	struct curl_httppost *lastptr = NULL;
	struct curl_slist *headerlist = NULL;
	static const char buf[] = "Expect:";

	// 报告编号
	curl_formadd(&formpost, &lastptr, 
		CURLFORM_COPYNAME, "reportNo", 
		CURLFORM_COPYCONTENTS, "123456789", 
		CURLFORM_END);

	// 图片1
	curl_formadd(&formpost, &lastptr, 
		CURLFORM_COPYNAME, "img", 
		CURLFORM_FILE, "pci.png", 
		//CURLFORM_CONTENTTYPE, "image/jpeg", 
		CURLFORM_END);

	// 图片2
	curl_formadd(&formpost, &lastptr, 
		CURLFORM_COPYNAME, "img", 
		CURLFORM_FILE, "cURLlibDemo.cpp", 
		//CURLFORM_CONTENTTYPE, "image/jpeg", 
		CURLFORM_END);

	curl = curl_easy_init();    // 初始化
	/* initalize custom header list (stating that Expect: 100-continue is not wanted */ 
	headerlist = curl_slist_append(headerlist, buf);	
	if (curl)
	{
		curl_easy_setopt(curl, CURLOPT_URL, "http://localhost:8080/UploadFile.ashx"); 
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
		curl_easy_setopt(curl, CURLOPT_HTTPPOST, formpost);				
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &strRet);	//将返回的html主体数据输出		
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, GetPageString);	//设置一个回掉函数来获取数据
		//curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);		// 输出日志
		res = curl_easy_perform(curl);   // 执行
		printf("curl_easy_perform返回：%d\r\n", res);

		curl_easy_cleanup(curl);   
		curl_formfree(formpost);
		curl_slist_free_all(headerlist);

		if (res == CURLE_OK) 
		{
		} 
	}
}

bool LdapLogin(std::string &strRet)
{
	CURL *curl;
	CURLcode nRetCode;
	
	std::string strBuf;
	curl = curl_easy_init();    // 初始化
	if (curl)
	{
		// 设置地址
		curl_easy_setopt(curl, CURLOPT_URL, "ldap://169.254.41.123");
		curl_easy_setopt(curl, CURLOPT_USERNAME, "liwr");
		curl_easy_setopt(curl, CURLOPT_PASSWORD, "90-poi");
		// 将返回的html主体数据输出		
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &strBuf);
		// 设置一个回掉函数来获取数据
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, GetPageString);
		// 设置超时
		curl_easy_setopt(curl, CURLOPT_TIMEOUT_MS, 10000);
		// 输出日志
		curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
		// 执行
		nRetCode = curl_easy_perform(curl);	
		// 清理
		curl_easy_cleanup(curl);	

		if (nRetCode == CURLE_OK)
		{
			return true;
		}
		else
		{
			printf("CURLcode(%d)\n", nRetCode);
		}
	}
	return false;
}

bool CurlPostJson(std::string &strRet)
{	
	CURL *curl;
	CURLcode nRetCode;
	struct curl_slist *headerlist = NULL;
	
	FILE *filep = NULL;
#ifdef DEBUG
	filep = fopen("curl.log", "a+");
#endif
	if (filep != NULL)
	{
		fprintf(filep, "\r\nCurlPostJson----------------------------------------------\r\n");
	}

	curl = curl_easy_init();    // 初始化
	headerlist = curl_slist_append(headerlist, "Expect:");	
	headerlist = curl_slist_append(headerlist, "Content-Type: application/json;charset=UTF-8");
	if (curl)
	{
		// 设置地址
		curl_easy_setopt(curl, CURLOPT_URL, "http://127.0.0.1:8080");
		// 自定义头
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
		// 设置Post的数据
		std::string strPostData = TCharToUTF8("粤Y1234511");
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, strPostData.c_str());
		// 将返回的html主体数据输出		
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, &strRet);
		// 设置一个回掉函数来获取数据
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, GetPageString);
		// 设置超时
		curl_easy_setopt(curl, CURLOPT_TIMEOUT_MS, 10000);
		
		if (filep != NULL)
		{
			curl_easy_setopt(curl, CURLOPT_STDERR, filep);
			curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
		}

		// 执行
		nRetCode = curl_easy_perform(curl);	
		// 清理
		curl_easy_cleanup(curl);	
		curl_slist_free_all(headerlist);
		
		if (filep != NULL)
		{
			fprintf(filep, "CurlPostJson----------------------------------------------\r\n");
			fclose(filep);
		}

		if (nRetCode == CURLE_OK)
		{
			return true;
		}
		else
		{
		}
	}
	if (filep != NULL)
	{
		fprintf(filep, "CurlPostJson----------------------------------------------\r\n");
		fclose(filep);
	}
	return false;
}

/* NOTE: if you want this example to work on Windows with libcurl as a
   DLL, you MUST also provide a read callback with CURLOPT_READFUNCTION.
   Failing to do so will give you a crash since a DLL may not use the
   variable's memory when passed in to it from an app like this. */ 
static size_t read_callback(void *ptr, size_t size, size_t nmemb, void *stream)
{
  curl_off_t nread;
  /* in real-world cases, this would probably get this data differently
     as this fread() stuff is exactly what the library already would do
     by default internally */ 
  size_t retcode = fread(ptr, size, nmemb, (FILE *)stream);
 
  nread = (curl_off_t)retcode;

  g_lUploadSize += (long)retcode;
  printf("上传进度=%10d/%d %5.2f%%\r", g_lUploadSize, g_lFileSize, (float)g_lUploadSize/g_lFileSize*100.0f);
  //fprintf(stderr, "*** We read %" CURL_FORMAT_CURL_OFF_T
  //        " bytes from file\n", nread);
  return retcode;
}

bool UploadFileFTP(LPCTSTR szFile)
{
	CURL *curl;
	CURLcode res;
	struct curl_slist *headerlist = NULL;

	std::string strBuf;
	curl = curl_easy_init();    // 初始化
    /* build a list of commands to pass to libcurl */ 
	//headerlist = curl_slist_append(headerlist, buf);	
	if (curl)
	{
		/* get a FILE * of the same file */ 
		FILE *fp = fopen("d:\\Boost_1.45.0@VS2005.7z", "rb");
		fseek(fp, 0, SEEK_END);
		curl_off_t fSize = (curl_off_t)ftell(fp);
		fseek(fp, 0, SEEK_SET);
		g_lFileSize = (long)fSize;

		curl_easy_setopt(curl, CURLOPT_URL, "ftp://admin:123@169.254.11.22/aa/bb/Boost_1.45.0@VS2005.7z"); 
		// 新建目录路径
		curl_easy_setopt(curl, CURLOPT_FTP_CREATE_MISSING_DIRS, CURLFTP_CREATE_DIR_RETRY);

		/* enable uploading */ 
		curl_easy_setopt(curl, CURLOPT_UPLOAD, 1L);
		
		/* now specify which file to upload */ 
		curl_easy_setopt(curl, CURLOPT_READDATA, fp);
		/* Set the size of the file to upload (optional).  If you give a *_LARGE
		option you MUST make sure that the type of the passed-in argument is a
		curl_off_t. If you use CURLOPT_INFILESIZE (without _LARGE) you must
		make sure that to pass in a type 'long' argument. */ 
		curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE, (curl_off_t)fSize);
		/* we want to use our own read function */ 
		curl_easy_setopt(curl, CURLOPT_READFUNCTION, read_callback);

		/* pass in that last of FTP commands to run after the transfer */ 
		//curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headerlist);
		curl_easy_setopt(curl, CURLOPT_TIMEOUT_MS, 60*1000);
		// 输出日志
		curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

		res = curl_easy_perform(curl);   // 执行

		curl_easy_cleanup(curl);  
		curl_slist_free_all(headerlist);

		fclose(fp);
		if (res == CURLE_OK) 
		{
		} 
	}
	return false;
}

int _tmain(int argc, _TCHAR* argv[])
{	
	std::string strRet;
	UploadFileXJ(strRet);
	//LdapLogin(strRet);
	//CurlPostJson(strRet);
	printf("返回：%s\r\n", KUTF8ToANSI(strRet.c_str()));
	
	//UploadFileFTP("");

	system("pause");
	return 0;
}

